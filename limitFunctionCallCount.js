function limitFunctionCallCount(cb,n){
    let count=0;
    function invokeCb(){
        if(count<=n){
            console.log(`${count}`)
            let invoke = cb()
            count++;
            return invoke
        }
        else {
        return null;
        }
    }
    return invokeCb;
}
module.exports = limitFunctionCallCount
