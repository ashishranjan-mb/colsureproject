function cacheFunction(cb) {
    let cache = {};
  
    function invokecb(obj){
      if(obj in cache) {
            return cache[obj];
      }
      else {
        const x = cb(obj);
        cache[obj] = x;
        return true
      }
    }
    return invokecb;
  }
  module.exports = cacheFunction;
  
