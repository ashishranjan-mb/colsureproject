const cacheFunction = require("../cacheFunction.js");
function cb(value) {
  return value * 6;
}
let data = cacheFunction(cb);
console.log(data(2, 4));
console.log(data(2));
