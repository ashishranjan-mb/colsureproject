function counterFactory() {
    let count = 0;
    return {
    increment () {
      return ++count;;
    },
    decrement () {
      return --count;
    }
}
}
module.exports = counterFactory;